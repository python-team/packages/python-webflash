Source: python-webflash
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper (>= 7),
 cdbs,
 python-dev,
 python-setuptools
Standards-Version: 3.8.2
Homepage: http://python-rum.org/wiki/WebFlash
Vcs-Git: https://salsa.debian.org/python-team/packages/python-webflash.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-webflash

Package: python-webflash
Architecture: all
Depends:
 ${misc:Depends},
 ${python:Depends},
 python (>=2.6) | python-simplejson
Conflicts: python-json
Description: Portable flash messages for Python WSGI applications
 WebFlash is a library to display "flash" messages in Python web
 applications.
 .
 These messages are usually used to provide feedback to the user (e.g:
 you changes have been saved, your credit card number has been stolen,
 etc). One important characteristic they must provide is the ability
 to survive a redirect (i.e., display the message in a page after
 being redirected from a form submission).
 .
  * WebFlash is implemented using cookies, no state must be kept
  * The messages can be rendered by the browser using Javascript
  * Alternatively messages can be rendered by the server
  * The JS code is small (~1Kb) and has no dependency
  * WebFlash is designed to be used in WSGI frameworks
